const express = require("express");
const fs = require("fs");
const amqp = require("amqplib/callback_api");

// Initializing the Express application.
const app = express();
const port = 8000;

// Middleware to parse plain text requests.
app.use(express.text());

app.post("/", (req, res) => {
  // Capture the client's IP address and port.
  const remoteAddress = req.socket.remoteAddress + ":" + req.socket.remotePort;

  // Send the constructed message to RabbitMQ.
  const responseMessage = req.body + " " + remoteAddress;
  sendToRabbitMQ('log', responseMessage);

  // Respond to the service1.
  res.send("OK");
});

/**
 * Function to send a message to RabbitMQ.
 * @param {string} topic - The topic to which the message should be published.
 * @param {string} message - The message to be sent.
 */
function sendToRabbitMQ(topic, message) {
  // Connect to RabbitMQ.
  amqp.connect('amqp://rabbitmq', (error, connection) => {
    // Create a channel.
    connection.createChannel((error, channel) => {
      const exchange = 'logs';

      // Publish the message to the specified exchange and topic.
      channel.assertExchange(exchange, 'topic', {durable: false});
      channel.publish(exchange, topic, Buffer.from(message));
    });
  });
}

// Delay the start of the Express server by 2 seconds.
setTimeout(() => {
  app.listen(port, () => {
    console.log(`Service2 listening at http://localhost:${port}`);
  });
}, 2000);

// Listening to RabbitMQ messages
amqp.connect('amqp://rabbitmq', (error, connection) => {
  if (error) {
    console.error("Failed to connect to RabbitMQ:", error);
    return;
  }

  // Create a channel.
  connection.createChannel((error, channel) => {
    if (error) {
      console.error("Failed to create a channel:", error);
      return;
    }

    const exchange = 'logs';
    channel.assertExchange(exchange, 'topic', {durable: false});

    // Declare an exclusive queue to bind and listen for messages.
    channel.assertQueue('', {exclusive: true}, (error, q) => {
      channel.bindQueue(q.queue, exchange, 'message');

      // Consume messages from the queue.
      channel.consume(q.queue, (msg) => {
        // Append "MSG" to the received message and send it to RabbitMQ.
        const responseMessage = msg.content.toString() + " MSG";
        sendToRabbitMQ('log', responseMessage);
      }, {noAck: true});
    });
  });
});

// Keep the service running...
setInterval(() => {
  console.log('Service 2 is running...');
}, 10000);
