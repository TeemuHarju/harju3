from flask import Flask, Response
import pika
import threading
import logging
import time

app = Flask(__name__)

# In-memory storage for logs.
logs = []

# URL of the RabbitMQ server.
RABBITMQ_URL = "rabbitmq"

@app.route('/', methods=['GET'])
def get_logs():
    """
    Endpoint to fetch the logs.
    
    Returns:
        Response: HTTP response containing the logs as plain text.
    """
    return Response('\n'.join(logs), mimetype='text/plain')

def callback(ch, method, properties, body):
    """
    Callback function that's triggered when a message is received from RabbitMQ.
    
    Parameters:
        body (bytes): The received message payload.
    
    Appends the received log message to the logs list.
    """
    logs.append(body.decode('utf-8'))

def consume_rabbitmq():
    """
    Function to set up and start consuming messages from RabbitMQ.
    """
    try:
        # Establish a connection to the RabbitMQ server.
        connection = pika.BlockingConnection(pika.ConnectionParameters(RABBITMQ_URL))
        channel = connection.channel()

        # Declare an exchange.
        channel.exchange_declare(exchange='logs', exchange_type='topic')

         # Declare an exclusive queue.
        result = channel.queue_declare('', exclusive=True)
        queue_name = result.method.queue

         # Bind the queue to the 'logs' exchange.
        channel.queue_bind(exchange='logs', queue=queue_name, routing_key='log')

        # Start consuming messages from the queue using the specified callback.
        channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)
        channel.start_consuming()
    except Exception as e:
        logging.error("Error in consume_rabbitmq: %s", e)

if __name__ == "__main__":
    # Start RabbitMQ consumer in a separate thread
    threading.Thread(target=consume_rabbitmq, daemon=True).start()
    
    # Start Flask app in a separate thread
    threading.Thread(target=lambda: app.run(host='0.0.0.0', port=8087), daemon=True).start()

    # Keep the main thread alive.
    while True:
        time.sleep(10)