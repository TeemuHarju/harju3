import time
from datetime import datetime
import requests
import pika
import logging

# Define URLs.
SERVICE_2_URL = "http://service2:8000"
RABBITMQ_URL = "rabbitmq"

# Establish a connection to RabbitMQ and declare an exchange.
connection = pika.BlockingConnection(pika.ConnectionParameters(RABBITMQ_URL))
channel = connection.channel()
channel.exchange_declare(exchange='logs', exchange_type='topic')

def sendToService(message):
    """Send a message to service2 via HTTP.
    
    On successful transmission, the HTTP status code is sent to RabbitMQ.
    If there's a failure, the error message is sent instead.

    Parameters:
    - message (str): The message to be sent to service2.
    
    Returns:
    - str: The text content of the response or None in case of an error.
    """
    try:
        # Define headers for the HTTP request.
        headers = {'Content-Type': 'text/plain'}

         # Send the message to service2.
        response = requests.post(SERVICE_2_URL, data=message, headers=headers)

        # If there's an issue with the response, an exception will be raised.
        response.raise_for_status()

        # Format the log message.
        log_message = f"{response.status_code} {datetime.utcnow().isoformat()}"

        # Publish the log message to RabbitMQ.
        channel.basic_publish(exchange='logs', routing_key='log', body=log_message)
        return response.text
    except requests.RequestException as e:
        # If there's an exception, publish it to RabbitMQ.
        channel.basic_publish(exchange='logs', routing_key='log', body=str(e))
        return None

def sendToRabbitMQ(topic, message):
    """Send a message to a specified topic on RabbitMQ.
    
    Parameters:
    - topic (str): The topic (routing key) to which the message should be published.
    - message (str): The actual message content to be sent.
    """
    channel.basic_publish(exchange='logs', routing_key=topic, body=message)

def service():
    """Function to handle the sending of messages."""

    # Initialize counter.
    counter = 1

    # Send 20 formatted messages to Service 2 and RabbitMQ.
    for _ in range(20):
        formatted_message = f"SND {counter} {datetime.utcnow().isoformat()} 192.168.2.22:8000"
        sendToRabbitMQ('message', formatted_message) # Sending to RabbitMQ
        sendToService(formatted_message)
        counter += 1
        time.sleep(2)

    # Signal the end of the process.
    sendToRabbitMQ('log', 'SND STOP')
    sendToService("STOP")

if __name__ == "__main__":

    service()

    #Keep the service running.
    while True:
        logging.info("Service2 is running...")
        time.sleep(10)
